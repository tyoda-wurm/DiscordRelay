# botToken: Discord bot token. This is the client secret that can be found when reviewing the information for your bot at the following link:
# https://discord.com/developers/applications/me
# The above link is also where you would create your bot, if you're unfamiliar with Discord bot creation.
# Image assistance: https://i.imgur.com/QDXT3VM.png
botToken=

# discordServerName: Discord server to connect to (not case sensitive).
discordServerName=ServerName

# useUnderscore: This will determine whether to replace white space with underscores or remove whitespace entirely.
useUnderscore=true

# showConnectedPlayers: Whether to show the number of players connected as the description of the bot in Discord.
showConnectedPlayers=true

# showConnectedPlayersMessage: The message to display the number of players with.
# NUM_PLAYERS will be replaced with the actual number.
# Probably shouldn't be too long.
# Only matters if showConnectedPlayers is set to true.
showConnectedPlayersMessage=NUM_PLAYERS players online!

# enableRumors: Whether to announce unique rumors to Discord.
enableRumors=true
# rumorChannel: Which channel to announce unique rumors to. Only applies if enableRumors is set to true
rumorChannel=rumors

# enableTrade: Whether to enable the global trade relay.
# Any messages sent to trade in-game will be relayed to a global #trade channel in Discord.
# Any messages sent to the #trade channel in Discord will be relayed in-game to all kingdom's trade chats.
enableTrade=true

# enableMGMT: Whether to enable MGMT relay
# Any message sent to MGMT in-game will be relayed to a global #mgmt channel in Discord.
# Any messages sent to the #mgmt channel in Discord will be relayed in-game to all players able to see the window.
# Permissions to see #mgmt in Discord must be configured by the server administrator.
# Permissions to see MGMT in-game is dictated by the in-game player status (CA, CM, GM, etc.)
enableMGMT=true

# enableCAHELP: Whether to enable CA HELP relay
# Any messages sent to CA HELP in-game will be relayed to #ca-help in Discord.
# Any messages sent to #ca-help in discord will be relayed in-game to CA HELP.
enableCAHELP=true

# Whether to count alts in the bot's presence "X players online!"
# true : the bot will report the number of characters on the server as the player number. (Counting alts as separate players)
# false: it will count the number of unique steam IDs on the server. (Thereby not counting alts as duplicate)
countAltsAsPlayers=true

# Whether to show attachments in-game
# true : attachments will show in a separate line after the message as "[image1.png], [funnyvideo.mp4], [malware.zip]"
# false: attachments will not be visible in-game
showAttachments=true

# Whether to show replies in-game
# true : A reply will be shown in the message like this: "<@DiscordUser1 to @DiscordUser2>" or,
#        if replying to the bot's message "<@DiscordUser1 to WurmPlayer>"
# false: The fact that a message is a reply will not be visible in-game, only the message itself
showReplies=true

# Whether to stop markdown formatting while sending messages from game to Discord
# true : Markdown characters will appear like normal characters, as they do in-game
# false: Markdown characters will format the text as they are meant to
noMarkdown=false
