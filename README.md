# DiscordRelay

Mod for Wurm Unlimited servers that allows the connection between in-game kingdom chat and a discord server.


# About this fork

This is a fork of jamesblack's repo, which was an improvement on Sindusk's DiscordRelay, which was an updated and
wyvernmods-compatible version of whisper2shade's original DiscordRelay mod.

## Important
For this version of the discord bot you need to enable "Message Content Intent" on the discord developer portal.
![](intent.png)

## Dependencies
For this version you must have the mod [SinduskLibrary](https://github.com/Sindusk/sindusklibrary/releases/latest) installed.

# The changes
 - Updated to the latest beta release of JDA, 5.0.0-beta.10
 - Fixed not sending messages from discord to wurm in ca-help
 - Added option to not count alts as separate players in the bot's presence text.
 - Added support for a handful of emojis, so they show up as ascii emoticons in-game ('😄'  will show up as ':D').